# INDITEX API REST
This application uses:
1. Maven build tool
2. Spring Framework + Java 9
3. Endpoint Test with MockMvc
4. Swagger documentation
5. H2 Database
6. Spring Actuator
7. Docker

# How to run it?

- execute: git clone https://gitlab.com/andresRolo/inditex.git in 'folder'

- move to 'folder' and execute: mvn clean install

- execute: docker build -t inditex:latest .

- execute: docker run -p 8080:8080 -t inditex

# How to verify it?

Access to Actuator http://localhost:8080/actuator/health URL

Access the documentation in http://localhost:8080/swagger-ui.html URL
