package com.inditex.prices.infraestructure.rest;

import com.inditex.prices.domain.Price;
import com.inditex.prices.dto.PriceResponse;
import com.inditex.prices.application.service.PricesService;
import com.inditex.prices.infraestructure.util.Constants;
import com.sun.istack.NotNull;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@ResponseBody
@RequestMapping(Constants.pricesPath)
@Api(value = Constants.pricesValue,
        tags = {Constants.version})
public class PricesController {

    private final PricesService pricesService;

    @Autowired
    public PricesController(PricesService pricesService) {
        this.pricesService = pricesService;
    }

    @GetMapping()
    @ApiOperation(value = Constants.getPricesValue)
    public ResponseEntity<PriceResponse> getPrices(@RequestParam(name = "brand_id") @NotNull Integer brandId,
            @RequestParam(name = "product_id") @NotNull Integer productId,
            @RequestParam(name = "application_date") @NotNull
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime applicationDate) {
        return ResponseEntity.ok(buildPriceResponseFrom(pricesService.getPrices(Price.builder()
                .brandId(brandId)
                .productId(productId)
                .applicationDate(applicationDate)
                .build())));
    }

    private PriceResponse buildPriceResponseFrom(Price price) {
        return PriceResponse.builder()
                .productId(price.getProductId())
                .brandId(price.getBrandId())
                .priceList(price.getPriceList())
                .startDate(price.getStartDate())
                .endDate(price.getEndDate())
                .price(price.getPrice())
                .build();
    }

}
