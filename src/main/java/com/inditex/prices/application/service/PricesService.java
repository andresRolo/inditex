package com.inditex.prices.application.service;

import com.inditex.prices.domain.Price;

public interface PricesService {

    Price getPrices(Price price);
}
