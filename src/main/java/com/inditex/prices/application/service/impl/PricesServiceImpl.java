package com.inditex.prices.application.service.impl;

import com.inditex.prices.infraestructure.db.entity.PriceEntity;
import com.inditex.prices.domain.Price;
import com.inditex.prices.application.repository.PricesRepository;
import com.inditex.prices.application.service.PricesService;
import com.inditex.prices.application.exception.NoPriceFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PricesServiceImpl implements PricesService {

    private final PricesRepository pricesRepository;

    @Autowired
    public PricesServiceImpl(PricesRepository pricesRepository) {
        this.pricesRepository = pricesRepository;
    }

    @Override
    public Price getPrices(Price price) {
        return pricesRepository.findAllByBrandIdAndProductIdAndEndDateGreaterThanEqualAndStartDateLessThanEqual(
                price.getBrandId(), price.getProductId(), price.getApplicationDate(),
                price.getApplicationDate())
                .stream()
                .reduce((a, b) -> a.getPriority() > b.getPriority() ? a : b)
                .map(this::buildPriceFrom)
                .orElseThrow(NoPriceFoundException::new);
    }

    private Price buildPriceFrom(PriceEntity price) {
        return Price.builder()
                .productId(price.getProductId())
                .brandId(price.getBrandId())
                .priceList(price.getPriceList())
                .startDate(price.getStartDate())
                .endDate(price.getEndDate())
                .price(price.getPrice())
                .build();
    }

}
