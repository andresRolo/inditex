package com.inditex.prices.application.exception;

public class NoPriceFoundException extends RuntimeException {

    public NoPriceFoundException() {
        super();
    }

}
